import 'package:flutter/material.dart';
import 'package:lubed/dashboard.dart';

void main() => runApp(MaterialApp(
      home: Signup(),
    ));

class Signup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up'),
        backgroundColor: const Color.fromARGB(255, 0, 120, 183),
        centerTitle: true,
      ),
      body: Column(mainAxisSize: MainAxisSize.min, children: [
        const SizedBox(
          height: 60,
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Name',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Surname',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Town/City',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Phone Number',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Email',
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(8.0),
          child: const TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password',
            ),
          ),
        ),
        Center(
          child: Expanded(
            child: ElevatedButton(
                onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Dashboard()))
                    },
                child: const Text(
                  'Create Account',
                )),
          ),
        ),
      ]),
    );
  }
}
