import 'package:flutter/material.dart';
import 'package:lubed/login.dart';

void main() => runApp(MaterialApp(
      home: Home(),
      debugShowCheckedModeBanner: false,
    ));

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 0, 120, 183),
          title: const Text('Welcome to Casa'),
          centerTitle: true,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              child: const Image(
                image: AssetImage('logo.png'),
                height: 145.0,
              ),
            ),
            Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 25.0, horizontal: 0.0),
                color: Color.fromARGB(255, 145, 224, 239),
                child: Center(
                  child: Text(
                    'Welcome to Casa',
                    style: TextStyle(
                      color: Color.fromARGB(700, 0, 120, 183),
                    ),
                  ),
                )),
            Container(
              padding: const EdgeInsets.all(20.0),
              color: const Color.fromARGB(255, 145, 224, 239),
              child: const Center(
                  child: Text(
                'Casa prouds it\'s self to be the best accommodation booking platform in South Africa',
                style: TextStyle(
                  color: Color.fromARGB(700, 0, 120, 183),
                ),
                textAlign: TextAlign.center,
              )),
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              color: const Color.fromARGB(255, 145, 224, 239),
              child: const Center(
                  child: Text(
                'SIGN IN NOW to get more of our offerings',
                style: TextStyle(color: Color.fromARGB(700, 0, 120, 183)),
                textAlign: TextAlign.center,
              )),
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              color: Colors.white,
              child: const Center(
                  child: Text(
                '',
                textAlign: TextAlign.center,
              )),
            ),
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Login()))
              },
              child: const Text(
                'SIGN IN',
              ),
            ),
            const SizedBox(
              height: 10.0,
            ),
          ],
        ));
  }
}
