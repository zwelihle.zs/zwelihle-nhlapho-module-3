import 'package:flutter/material.dart';
import 'package:lubed/feature.dart';

void main() => runApp(const MaterialApp(
      home: Profile(),
    ));

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 0, 120, 183),
        title: const Text('Profile Edit'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Center(
            child: SizedBox(
              height: 114,
              width: 114,
              child: Stack(
                fit: StackFit.expand,
                clipBehavior: Clip.none,
                children: [
                  const CircleAvatar(
                    backgroundImage: AssetImage('assets/man.png'),
                  ),
                ],
              ),
            ),
          ),
          Column(
            children: [
              const Padding(padding: EdgeInsets.all(25)),
              Container(
                child: const Text(
                  'Name : Ziyanda',
                  textAlign: TextAlign.start,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: const Text('Surname : Malabu'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(child: const Text('Phone : 023 9520 691')),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                    child: const Text('Email : malibuZiya@wenmail.com')),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(child: const Text('Location/Town : Khayalam')),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(child: const Text('Country : South Africa')),
              ),
            ],
          ),
          ElevatedButton(
            onPressed: () => {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Feature()))
            },
            child: const Text(
              'Home',
            ),
          ),
        ],
      ),
    );
  }
}
